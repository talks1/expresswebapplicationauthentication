# Express WebApplication Authentication

Most applications we create are likely to require some access restrictions and this requires a method to authenticate users.

By adding this capability to the `real-value-full-stack web-application` in the [project generator](https://gitlab.com/q1-packages/generator) this should be easily acheivable for new green field implementations.

## Approaches to authentication

There are been multiple approaches to authentication
- MineQ - Identity Server back end with open id connect
- CoatesHire - Identity Server back end with open id connect
- Arena - Azure AD back end via passport-azure-ad and open id connect
- Ground Gauge - MSAL integration to Azure AD and Microsoft Graph API
- Cluster - python specific security mechanism (werkzeug)

## Approach Chosen

For addition into the web-application project generator, the approach of passport and open id connect has been selected.

Passport is a technology choice abstraction a layer which
- is well established
- open source
- can be very simple
- support multiple authentication providers (github, google, facebook, twitter)
- supports local database options

## Summary

- An authentication.js file has been added to the source code
- In azure you need to add a registered application and copy the client id and secret into the application configuration...to establish trust
- Some additional dependencies ... which can be added to the archtectype
- Individual routes can be protected by the addition of `isLoggedIn`  
```
  app.post('/api/jobs/:currentWeek', isLoggedIn, (req, res) => {...}
```

##  Component Intereaction Viewpoint

There are 5 separate components in the web application.

- Browser
- Front End Static Site
- Web Server
- API
- Azure AD

![](./images/Components.PNG)

The components interact in the following way.

![](./images/Sequence.PNG)

## Code Viewpoint

### Dependencies

```
const session = require('express-session');
const memoryStore = require('memorystore')(session);
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const passport = require('passport');
const OIDCStrategy = require('passport-azure-ad').OIDCStrategy;
```

`express-session` -  This populates/extracts the session id from client request/response.
`memory-store` - This is used to maintain a server side session.
`bodyParser` - This is used to extract the open id connect id-token from the response from Azure AD
`cookieParser` - This is used to parse/populate cookies in the client requests/responses  
`passport` - This is the abstraction framework which primarily provides the `passport.authenticate` middleware
`passport-azure-ad` - This provides the implementation which is able to generate open id request to AzureAD and parse the AzureAD callback payload.

### Configuration

In order to connect to Azure AD an `enterprise application` needs to be added into Azure AD.
A significant part of this configuration is the `callback url` which is the end point that Azure AD should redirect to once the user is authenticated. It is the endpoint in the node js implementation that establishes the web applicaton session.

When the `application registration` is created in Azure AD there are client id and client secrets which are used in the web application and allow Azure AD to trust the incoming Open ID request.

![](./images/AppRegistration.PNG)

![](./images/AppRegistrationCallback.PNG)

You will need to create a client secret in the portal and refer to this in the node js authentication configuration.

![](./images/AppRegistrationSecret.PNG)

Finally you need to enable the implicit grant id token as we are requested the id token (which include some user details)

![](./images/ImplicitGrant.PNG)
```
var config = { 
    creds: {
        ...     
        responseType: 'id_token code', // for login only flows use id_token. For accessing resources use `id_token code`
        ...
    }
    }
```
### Serialization/Deserialization

As a session is establish, passport provides a serialize and deserialize callback to be registerd.
The `serialize` callback is used when user logs into and can populate into the session additional details of the user.
For example
- a profile picture for the user
- permissions for the user (in order to provide for fine grained access)

As each API request is received the `deserialize` callback is callck to retrieve details of the user which are then automatically added to the request object as `req.user`.  End point middleware then has access to the user details to perform any authorization logic.

```
passport.serializeUser(function (user, done) {
    debug(`Serialize: ${user}`)
    done(null, user.upn ? user.upn : user._json.email);
});

passport.deserializeUser(function (email, done) {
    debug(`DeSerialize: ${email}`)  
    done(null, email)
})
```

### Handling XHR failures

The only change required to the web application to support the authentication mechanism is to handle API failures when the request contains no session cookie or an expired session cookie and redirect to the login end point.

To do so, a generic response handler is defined which captures the 401 response from any endpoint and redirects to the login end point.
```
let handleResponse = res =>{ 
    if(res.status===401){
        window.location = 'http://localhost:1111/auth/login'
    }else {
        return res.json()
}}
```

This handler is used for all api requests.
```
const service = {
    get: (url) => fetch(getAPI() + url, {credentials: 'include'}).then(handleResponse),
    put: (url, body) => fetch(getAPI() + url, { credentials: 'include',method: 'PUT', body: JSON.stringify(body) }).then(handleResponse),
    delete: (url) => fetch(getAPI() + url, { credentials: 'include',method: 'DELETE' }).then(handleResponse),
    post: (url, body) => fetch(getAPI() + url, { credentials: 'include',method: 'POST', body: JSON.stringify(body) }).then(handleResponse),
}
```


# Appendix

## Sequence Diagram
<pre>
title Express Web Application Authentication

Browser->WebServer: Get / (HTTP Request)
WebServer->StaticSite:
WebServer -> Browser: Static site returned
Browser->API: Get some data (XHR Request)
note right of API: Check if user logged in
API -> Browser:  302 Redirect to /auth/login
Browser->API: GET /auth/login
note right of API: Check if user authenticated
API->Browser: Redirect to Azure AD
Browser->AzureAD: GET /login
note right of AzureAD: Allow user to log in
AzureAD->Browser: 302 Redirect to /auth/login/callback
Browser->API: GET /auth/login/callback
note right of API: Creating server side session
API -> Browser:  302 Redirect to /
Browser->API: Get some data (XHR Request with session cookie)
API -> Browser:  Responds with data
</pre>

### Component Source
<pre>
dot`digraph {
rankdir="LR"
 compound=true

Browser -> WebServer [label="Make HTTP/XHR request sending cookies"]
Browser -> AzureAD [ label="Make open id request"]
Browser -> Browser [ label="Render UI"]

WebServer-> App [label="Read static site" lhead=clusterStaticSite]
WebServer-> Session [label="Forward /api and /auth Requests to API" lhead=clusterAPI]

subgraph clusterAPI {
  label = "API";
  Session
  SlashAPI -> LoginRedirect
  SlashAPI -> ServeData
  SlashLogin -> AzureRedirect
  SlashLogin -> AppRedirect
  SlashLoginCallback -> AppRedirect
  
  Fetch -> Session [lhead=clusterAPI]
}

subgraph clusterStaticSite {
  label = "StaticSite";
  Layout 
  Components 
  App
  Model -> Fetch
  Actions -> Fetch
}

}`
</pre>



